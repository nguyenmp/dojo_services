module.exports = function(app){
  var exec = require('child_process').exec;
  var fs = require('fs');
  app.post("/getDiscussionNames", function(req, res){
    var url = req.body.url
    var date = req.body.date
    var sheetId = req.body.sheetId
    var sheetName = req.body.sheetName
    var cell = req.body.cell
  
  
    exec(`/home/ubuntu/.nvm/versions/node/v8.8.1/bin/casperjs ./scripts/getDiscussionNames.js ${date} ${url}`, (err, stdout, stderr) => {
      if (err) {
        console.error("jere",err);
        return;
      }
      var array = stdout.split(",")
      
      for (let index = 0; index < array.length; index++) {
          var element = array[index];
          array[index] = [element]
          console.log(element)
      }
      res.json({data:array})
      // Load client secrets from a local file.
    });
  })
  app.post("/getAssignments", function(req, res){
    var stack_data = {
      "python":10,
      "webfundamentals":9,
      "MEAN":11,
      "C#":25
    }
    console.log(req.body)
    var emails = req.body["emails[]"]
    emails = req.body["emails[]"].join(" ")
    emails += " nvnguyen90@gmail.com"
    emails = emails.replace(/["'(]/g, "\\(").replace(/["')]/g, "\\)")
    var stack = stack_data[req.body.stack]
    console.log(`casperjs ./scripts/getAssignments.js ${emails} --stack-index=${stack}`)
    exec(`casperjs ./scripts/getAssignments.js ${emails} --stack-index=${stack}`, (err, stdout, stderr) => {
      if (err) {
        console.error("error:",err);
        return;
      }
      console.log(stdout.substring(stdout.indexOf("DATA INCOMING")+"DATA INCOMING".length, stdout.indexOf("DATA ENDING")))
      var data = stdout.substring(stdout.indexOf("DATA INCOMING")+"DATA INCOMING".length+1, stdout.indexOf("DATA ENDING")-1)
      res.json({"data":JSON.parse(data)})
    })
  })
  app.get("/changeDiscussionDates", function(req, res){
    var stack = req.body.stack
    var date = req.body
    exec(`casperjs ./scripts/changeDiscussionTopicDates.js --stack-index=${stack} --start-date=${date}`, (err, stdout, stderr) => {
      if (err) {
        console.error("error:",err);
        return;
      }
      // console.log(stdout)
      console.log(stdout.substring(stdout.indexOf("DATA INCOMING")+"DATA INCOMING".length, stdout.indexOf("DATA ENDING")))
      var data = stdout.substring(stdout.indexOf("DATA INCOMING")+"DATA INCOMING".length+1, stdout.indexOf("DATA ENDING")-1)
      res.json({"result":true})
    })
  })
}

//     var readline = require('readline');
// var google = require('googleapis');
// // console.dir(google.google.sheets())
// var OAuth2Client = google.google.auth.OAuth2;
// var SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
// var TOKEN_PATH = './assets/credentials.json';
// fs.readFile('./assets/client_secret.json', (err, content) => {
//   if (err) return console.log('Error loading client secret file:', err);
//   console.log(JSON.parse(content))
//   console.log("========================", array)
//   // Authorize a client with credentials, then call the Google Sheets API.
//   authorize(JSON.parse(content), listMajors.bind(array));
// });

// /**
//  * Create an OAuth2 client with the given credentials, and then execute the
//  * given callback function.
//  * @param {Object} credentials The authorization client credentials.
//  * @param {function} callback The callback to call with the authorized client.
//  */
// function authorize(credentials, callback) {
//   console.log(credentials.web)
//   var {client_secret, client_id, redirect_uris} = credentials.web;
//   console.log("here")
//   var oAuth2Client = new OAuth2Client(client_id, client_secret, redirect_uris[0]);
//   console.log("here1")
//   // Check if we have previously stored a token.
//   fs.readFile(TOKEN_PATH, (err, token) => {
//     console.log(TOKEN_PATH)

//     if (err) return getNewToken(oAuth2Client, callback);
//     console.log("here3")
//     oAuth2Client.setCredentials(JSON.parse(token));
//     oAuth2Client.credentials = JSON.parse(token)
//     console.log("here4")
//     callback(oAuth2Client);
//   });
// }

// /**
//  * Get and store new token after prompting for user authorization, and then
//  * execute the given callback with the authorized OAuth2 client.
//  * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
//  * @param {getEventsCallback} callback The callback for the authorized client.
//  */
// function getNewToken(oAuth2Client, callback) {
//   var authUrl = oAuth2Client.generateAuthUrl({
//     access_type: 'offline',
//     scope: SCOPES,
//   });
//   console.log('Authorize this app by visiting this url:', authUrl);
//   var rl = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout,
//   });
//   rl.question('Enter the code from that page here: ', (code) => {
//     rl.close();
//     oAuth2Client.getToken(code, (err, token) => {
//       if (err) return callback(err);
//       oAuth2Client.setCredentials(token);
//       // Store the token to disk for later program executions
//       fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
//         if (err) console.error(err);
//         console.log('Token stored to', TOKEN_PATH);
//       });
//       callback(oAuth2Client);
//     });
//   });
// }

// /**
//  * Prints the names and majors of students in a sample spreadsheet:
//  * @see https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
//  * @param {OAuth2Client} auth The authenticated Google OAuth client.
//  */
// var values = array
// var body = {
//   values: values
// };
// console.log(array)
// function listMajors(auth) {
//     console.log(this)
//   var sheets = google.google.sheets({version: 'v4', auth});
//   console.log(array)
//   sheets.spreadsheets.values.update({
//       // spreadsheetId: '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms',
//       spreadsheetId:sheetId,
//       range: `${sheetName}!${cell}`,
//       resource: body,
//       valueInputOption:"RAW"
//     }, (err, data) => {
//       if(err) {
//           // Handle error
//           console.log(err);
//         } else {
//           console.log('%d cells updated.', data.updatedCells);
//           res.send({data:array.map(x=>x[0])})
//         }
//     });
// }