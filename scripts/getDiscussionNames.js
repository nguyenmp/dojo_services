var casper = require('casper').create();
var env = require('system').env
var CDEMAIL = env["CDEMAIL"]
var CDPASS = env["CDPASS"]
casper.options.waitTimeout = 20000
casper.start('http://learn.codingdojo.com/');

var url = casper.cli.args[1]
casper.thenEvaluate(function(email, password) {
	document.getElementById("enter_email").value = email
    document.getElementById("enter_password").value = password
    document.getElementById("login_button").click()
}, CDEMAIL, CDPASS);
casper.waitForSelector("#tracks", function(){
})
casper.thenOpen("https://learn.codingdojo.com/d/"+url, function(){
    var date = casper.cli.args[0]
    date = date.replace(/[\r\n]/g, '')
    var data = this.getElementsInfo("#discussion_response_list")[0].html.split('</li>')
    data.pop()
    var names = []
    for(var i = 0 ; i < data.length; i++){
        var startOfName = data[i].indexOf("responded_by")+2+"responded_by".length
        var endOfName =  data[i].indexOf("</span>", startOfName)
        var start_of_date = data[i].indexOf('responded_at">') + 'responded_at">'.length
        var end_of_date = data[i].indexOf("</span>", start_of_date)
        if(new Date(data[i].substring(start_of_date, end_of_date)) >= new Date(date)){
            names[i] = data[i].substring(startOfName, endOfName)
        }else{
            break;
        }
    }
    this.echo(names)
})

casper.then(function(){

})
casper.run();