var casper = require('casper').create();
var env = require('system').env
var CDEMAIL = env["CDEMAIL"]
var CDPASS = env["CDPASS"]
casper.options.waitTimeout = 60000
casper.start('http://learn.codingdojo.com/');
var url = casper.cli.args[1]

casper.thenEvaluate(function(email, password) {
	document.getElementById("enter_email").value = email
    document.getElementById("enter_password").value = password
    document.getElementById("login_button").click()
}, CDEMAIL, CDPASS);
casper.waitForSelector("#tracks", function(){
})
casper.thenOpen("http://learn.codingdojo.com/admin/progress")
casper.waitWhileSelector('.table_body_container.table_loading_bg', function(){
    this.echo("cleared")
});
casper.thenEvaluate(function(stack){
    document.getElementsByClassName("btn-group bootstrap-select product_select")[0].children[1].children[0].children[0].children[0].click()
    document.getElementsByClassName("btn-group bootstrap-select stack_list_select")[0].children[1].children[0].children[0].children[0].click()
    document.getElementsByClassName("btn-group bootstrap-select dates_select")[0].children[1].children[0].children[0].children[0].click()
    setTimeout(function(){
        document.querySelectorAll(".btn-group.bootstrap-select .dropdown-menu.open")[5].children[0].children[stack].children[0].click()
    }, 500)
    document.getElementById("search_progress_students_input").value = "eugenemu55@gmail.com"
    document.getElementsByClassName("btn btn-primary fetch_filter_btn")[0].click()
}, casper.cli.options["stack-index"])
casper.waitFor(function check() {
    return this.evaluate(function() {
        return document.querySelectorAll("#progress_student_table_wrapper > div > div.DTFC_LeftWrapper > div.DTFC_LeftBodyWrapper > div > table > tbody > tr.odd[user-id='10244']").length > 0;
    });
})
casper.thenEvaluate(function(emails, stack){

    function cleanString(str) {
        str = str.replace(/\s+/g,"");
        str = str.trim()
        return str.toLowerCase()
    }
    function newData(email, stack){
        document.getElementsByClassName("btn-group bootstrap-select dates_select")[0].children[1].children[0].children[0].children[0].click()
        document.getElementsByClassName("btn-group bootstrap-select product_select")[0].children[1].children[0].children[0].children[0].click()
        document.getElementsByClassName("btn-group bootstrap-select stack_list_select")[0].children[1].children[0].children[0].children[0].click()
        document.querySelectorAll(".btn-group.bootstrap-select .dropdown-menu.open")[5].children[0].children[stack].children[0].click()
        document.getElementById("search_progress_students_input").value = email
        document.getElementsByClassName("btn btn-primary fetch_filter_btn")[0].click()
    }
    var required_text_data = {
        10:"Python / Django Online",
        9:"Web Fundamentals Online",
        11:"MEAN Online",
        25:"C# / .NET Core Online"
    }
    var required_text = required_text_data[stack]
    var dataSet = []
    function waitForElementToDisplay(selector, index, tries) {
        if(emails[index] == "nvnguyen90@gmail.com"){
            return
        }
        if(tries > 6){
            var data = {
                "email":emails[index],
                "current":"",
                "latest":{ 
                    "name": "",
                    "completed_dated": ""
                },
                "num_completed":"",
                "num_completed_optionals": ""
            }
            dataSet.push(data)
            if(emails[index + 2] == "nvnguyen90@gmail.com"){
                console.log("DATA INCOMING")
                console.log(JSON.stringify(dataSet))
                console.log("DATA ENDING")
            }
            newData(emails[index+2], stack)
            setTimeout(function(){
                waitForElementToDisplay(".table_body_container.table_loading_bg",index+2,1)
            }, 500)
        }else if(
            document.querySelector(selector) == null 
            && document.querySelector(".current_assignment") != null 
            && document.querySelector("#filter_admin_students_form > ul.list-unstyled.toolbar_list > li.pull-left > div > div > ul > li.selected").dataset["originalIndex"] == stack
            && document.querySelector("#filter_admin_students_form > ul.list-unstyled.toolbar_list > li.pull-left > div > button > span.filter-option.pull-left").innerText == required_text
            && document.querySelector("#progress_student_table_wrapper > div > div.DTFC_LeftWrapper > div.DTFC_LeftBodyWrapper > div > table > tbody > tr > td > span") != null
            && cleanString(document.querySelector("#progress_student_table_wrapper > div > div.DTFC_LeftWrapper > div.DTFC_LeftBodyWrapper > div > table > tbody > tr > td > span").innerText) == cleanString(emails[index+1])
        ) {
                var assignments = document.querySelectorAll(".list-unstyled.student_progress_list li.completed")
                if (assignments.length == 0){
                    var data = {
                        "email":emails[index],
                        "current":"",
                        "latest":{ 
                            "name": "",
                            "completed_dated": ""
                        },
                        "num_completed":"",
                        "num_completed_optionals": ""
                    }
                }else{
                    var data = {                        
                        "email":emails[index],
                        "current":document.querySelector(".current_assignment").innerText,
                        "latest":{ 
                            "name": assignments[0].dataset.assignment,
                            "completed_date": new Date(assignments[0].dataset.completedAt)
                        },
                        "num_completed":assignments.length
                    }
                    var num_of_optionals = 0
                    for(var i = 1 ; i < assignments.length; i++){
                        var date = new Date(assignments[i].dataset.completedAt)
                        if (data.latest.completed_date < date){ 
                            data.latest.name = assignments[i].dataset.assignment
                            data.latest.completed_date = date
                        }
                        if (assignments[i].dataset.assignment.toLowerCase().indexOf("optional") >= 0){
                            num_of_optionals += 1
                        }
                    }
                    data["num_completed_optionals"] = num_of_optionals
                }
                // __utils__.echo(JSON.stringify(dataSet))
                dataSet.push(data)
                if(emails[index + 2] == "nvnguyen90@gmail.com"){
                    console.log("DATA INCOMING")
                    console.log(JSON.stringify(dataSet))
                    console.log("DATA ENDING")
                }
                newData(emails[index+2], stack)
                setTimeout(function(){
                    waitForElementToDisplay(".table_body_container.table_loading_bg",index+2,1)
                }, 500)
                
        }else {
            setTimeout(function(){
                waitForElementToDisplay(selector, index, tries+1);
            }, 500)
        }
    }
    newData(emails[0], stack)
    setTimeout(function(){
        waitForElementToDisplay(".table_body_container.table_loading_bg", 0, 1)
    },500)

}, casper.cli.args, casper.cli.options["stack-index"])
casper.waitFor(function check() {
    return this.evaluate(function() {
        return document.querySelectorAll("#progress_student_table_wrapper > div > div.DTFC_LeftWrapper > div.DTFC_LeftBodyWrapper > div > table > tbody > tr.odd[user-id='10989']").length > 0;
    });
}, function then(){
    // this.echo("finished")
})
casper.on('remote.message', function(msg) {
    this.echo(msg);
})
casper.on('page.error',function (msg, trace) {
    this.echo( 'Error: ' + msg, 'ERROR' );
})

casper.run();