var casper = require('casper').create();
var urls = {
	"MEAN":"https://learn.codingdojo.com/d/14",
	"C#":"https://learn.codingdojo.com/d/58/",
	"webfundamentals":"https://learn.codingdojo.com/d/12",
	"python":"https://learn.codingdojo.com/d/13"
}

var base_url = urls[casper.cli.options["stack-index"]]
var start = casper.cli.options["start-date"]
var dates = []
var curr = new Date(start)
casper.options.waitTimeout = 20000
var formatDate = function(dateObj){
	if(typeof(dateObj) == "string") dateObj = new Date(dateObj)
	var dd = dateObj.getDate();
	var mm = dateObj.getMonth() + 1;
	var y = dateObj.getFullYear();
	var someFormattedDate = mm + '/'+ dd + '/'+ y;
	return someFormattedDate
}
casper.start('http://learn.codingdojo.com/');
casper.thenEvaluate(function(email, password) {
	document.getElementById("enter_email").value = email
    document.getElementById("enter_password").value = password
    document.getElementById("login_button").click()
}, 'mnguyen@codingdojo.com', 'nguyenmp93');
casper.waitForSelector("#tracks", function(){
	this.echo("waiting")
	this.echo(this.getCurrentUrl())
})
casper.thenOpen(base_url, function(){
	var main = []
	// this.echo(JSON.stringify(this.getElementsInfo(".tab_title")))
	for (var i = 0; i < 8; i++) {
		if( i == 4) curr = new Date(start)
		main.push({link:this.getElementsInfo(".tab_title")[i].attributes.href, date:formatDate(curr)})
		curr.setDate(curr.getDate() + 7)
	}
	// console.log(JSON.stringify(main))
	this.eachThen(main, function(response) {
		console.log("response.data:", JSON.stringify(response.data))
		var curr = new Date(response.data.date)
	  	this.thenOpen("https://learn.codingdojo.com"+response.data.link, function() {
			var links = []
	  		for (var i = 0; i < this.getElementsInfo(".sub_tab_btn").length; i++) {
				links.push({link:this.getElementsInfo(".sub_tab_btn")[i].attributes.href, date:formatDate(curr)})
				curr.setDate(curr.getDate() + 1)
			}
			this.echo(links)
			this.each(links, function(self, data) {
			    self.thenOpen("https://learn.codingdojo.com"+data.link, function() {
			        this.echo(this.getCurrentUrl());
			    });
		  		casper.thenEvaluate(function(){
	      			document.getElementsByClassName("edit_discussion_btn")[0].click()
		  		})
			    casper.waitUntilVisible("#manage_discussion_form", function(){
					this.echo("form showing")
				})

				this.echo(data.date)
				casper.thenEvaluate(function(today_date){
					document.getElementById("response_due_date").children[0].value = today_date
					var current_date = document.getElementById("response_due_date").children[0].value.split("/")
					current_date = current_date.map(function(x){ return parseInt(x)})
					current_date[0] = 6
					current_date[1] = current_date[1] - 2
					// if (current_date[1] > 6){
					// 	current_date[1] = current_date[1] - 3
					// }
					// if(current_date[1] > 28){
					// 	current_date[0] = 2
					// 	current_date[1] = current_date[1] - 31
					// }
					document.getElementById("response_due_date").children[0].value = current_date.join("/")
					document.getElementsByClassName("modal-footer")[0].children[1].click()
				}, formatDate(data.date))
			})
		});
	});
})

casper.then(function(){

})
casper.run();