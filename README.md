# INFO
This is a repository set up to run micro service mostly for online coding dojo program instructors

# SETUP
1. Be sure to update environment variables. Needed are: 
export CDEMAIL=mnguyen@codingdojo.com
export CDPASS=<codingdojo password>
export GMAILPASS=<gmail password> _for emailing_
export MMPASS=<mattermost password> _mattermost functionalities_

# Flightplan
Flight plan is a simple way to deploy the website and to get it on aws without having to ssh into the terminal. Read more here : https://github.com/pstadler/flightplan
``` 
plan.target('testing', {
    host: '<public EC2 DNS>',
    username: 'ubuntu',
    agent: process.env.SSH_AUTH_SOCK,
    repository: 'https://gitlab.com/nguyenmp/dojo_services.git',
    branch: 'master',
    privateKey: '<location of PEM key>',
    maxDeploys: 10
});
```
